from socket import *
import threading
import json
import os
import shutil
from user import *
import base64
import datetime
import time


MAX_SIZE = 2048
LOGGED_OUT = 0
WAITING_FOR_PASSWORD = 1
LOGGED_IN = 2
SYNTAX_ERROR = "501 Syntax error in parameters or arguments."
OTHER_ERROR = "500 Error."
NOT_ALLOWED = "550 File unavailable."
BASE_DIR = os.getcwd()

class Server:
    
    def __init__(self):
        self.loadConfiguration()
        self.startServer()

    def searchUser(self, name):
        for user in self.users:
            if user.username == name:
                return user
        return None

    def isAllowed(self, currentUser, fileName):
        if not self.authorization:
            return True
        elif currentUser.admin:
            return True
        elif fileName not in self.privilegedFiles:
            return True
        else:
            return False

    def sendEmail(self, currentUser):
        if not currentUser.alert:
            return
        msg = "Your traffic ran below threshold.\r\nYour remained traffic is "
        msg += str(currentUser.size)
        msg += " bytes."
        endmsg = "\r\n.\r\n"
        mailserver = ("mail.ut.ac.ir", 25)
        serverMailSocket = socket(AF_INET, SOCK_STREAM)
        serverMailSocket.connect(mailserver)
        recv = serverMailSocket.recv(1024).decode('utf-8')
        print("Message after connection request:" + recv)
        if recv[:3] != '220':
            print('220 reply not received from server.')
        heloCommand = 'HELO Fatemeh\r\n'
        serverMailSocket.send(heloCommand.encode('utf-8'))
        recv1 = serverMailSocket.recv(1024).decode('utf-8')
        print("Message after HELO command:" + recv1)
        if recv1[:3] != '250':
            print('250 reply not received from server.')

        #Info for username and password
        username = "f.alagheband"
        password = "MFfm3722119"
        base64_str = ("\x00"+username+"\x00"+password).encode()
        base64_str = base64.b64encode(base64_str)
        authMsg = "AUTH PLAIN ".encode()+base64_str+"\r\n".encode()
        serverMailSocket.send(authMsg)
        recv_auth = serverMailSocket.recv(1024)
        print(recv_auth.decode())

        mailFrom = "MAIL FROM:<f.alagheband@ut.ac.ir>\r\n"
        serverMailSocket.send(mailFrom.encode())
        recv2 = serverMailSocket.recv(1024)
        recv2 = recv2.decode()
        print("After MAIL FROM command: "+recv2)
        rcptTo = "RCPT TO:<"+ currentUser.email + ">\r\n"
        serverMailSocket.send(rcptTo.encode())
        recv3 = serverMailSocket.recv(1024)
        recv3 = recv3.decode()
        print("After RCPT TO command: "+recv3)
        data = "DATA\r\n"
        serverMailSocket.send(data.encode())
        recv4 = serverMailSocket.recv(1024).decode('utf-8')
        print("After DATA command: "+recv4)
        mailInfo = "From: f.alagheband@ut.ac.ir\r\n"
        serverMailSocket.send(mailInfo.encode())
        mailInfo = "To: "+ currentUser.email +"\r\n"
        serverMailSocket.send(mailInfo.encode())
        mailInfo = "Subject: Accounting threshold reached.\r\n\r\n" 
        serverMailSocket.send(mailInfo.encode())
        date = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
        date = date + "\r\n\r\n"
        serverMailSocket.send(date.encode())
        serverMailSocket.send(msg.encode())
        serverMailSocket.send(endmsg.encode())
        recv_msg = serverMailSocket.recv(1024)
        print("Response after sending message body:"+recv_msg.decode())
        quitMessage = "QUIT\r\n"
        serverMailSocket.send(quitMessage.encode())
        recv5 = serverMailSocket.recv(1024)
        print(recv5.decode())
        serverMailSocket.close()

    def hasTraffic(self, currentUser, fileName):
        if not self.accounting:
            return True, currentUser
        path = os.path.join(BASE_DIR,fileName)
        fileSize = os.stat(path).st_size
        if currentUser.size - fileSize > 0:
            currentUser.size = currentUser.size - fileSize
            if currentUser.size < self.threshold:
                self.sendEmail(currentUser)
            return True, currentUser
        return False, currentUser

    def writeLog(self, message):
        if (not self.logging) or message == "":
            return
        logFileAddress = ""
        if self.loggingPath.startswith("./"):
            logFileAddress = BASE_DIR + self.loggingPath[1:]
        else:
            logFileAddress = BASE_DIR + self.loggingPath
        logFile = open(logFileAddress, "a")
        date = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
        logFile.write(date + " " + message + "\n")
        logFile.close()

    def loadConfiguration(self):
        configFile = open("config.json").read()
        configs = json.loads(configFile)
        self.assignConfigs(configs)

    def assignConfigs(self, configs):
        self.commandChannel = configs['commandChannelPort']
        self.dataChannel = configs['dataChannelPort']
        self.users = []
        for user in configs['users']:
            newUser = User(user['user'], user['password'])
            self.users.append(newUser)
        self.setAccounting(configs['accounting'])
        self.setLogging(configs['logging'])
        self.setAuthorization(configs['authorization'])
          
    def setAccounting(self, configs):
        self.accounting = configs['enable']
        if not self.accounting:
            return
        self.threshold = configs['threshold']
        for user in configs['users']:
            foundUser = self.searchUser(user['user'])
            if foundUser != None:
                foundUser.setAccountingInfo(user['size'], user['email'], user['alert'])

    def setLogging(self, configs):
        self.logging = configs['enable']
        if not self.logging:
            return
        self.loggingPath = configs['path']
        logFile = open(self.loggingPath, "w")
        logFile.close()

    def setAuthorization(self, configs):
        self.authorization = configs['enable']
        if not self.authorization:
            return
        for admin in configs['admins']:
            foundUser = self.searchUser(admin)
            if foundUser != None:
                foundUser.setAdmin()
        self.privilegedFiles = configs['files']

    def handleUserCommand(self, commandTokens, currentUser):
        if len(commandTokens) != 2:
            return [SYNTAX_ERROR, ""], LOGGED_OUT, currentUser
        foundUser = self.searchUser(commandTokens[1])
        if foundUser == None:
            return ["430 Invalid username or password.", ""], LOGGED_OUT, currentUser
        logMessage = "Username " + commandTokens[1] + " is entering."
        return ["331 User name okay, need password.", logMessage], WAITING_FOR_PASSWORD, foundUser

    def handlePassCommand(self, commandTokens, currentUser):
        if len(commandTokens) != 2:
            return [SYNTAX_ERROR, ""], WAITING_FOR_PASSWORD, currentUser
        if currentUser.password != commandTokens[1]:
            return ["430 Invalid username or password.", ""], WAITING_FOR_PASSWORD, currentUser
        logMessage = "User " + currentUser.username + " entered."
        return ["230 User logged in, proceed.", logMessage], LOGGED_IN, currentUser

    def handleQuit(self, commandTokens, currentUser):
        if len(commandTokens) != 1:
            return [SYNTAX_ERROR, ""], LOGGED_OUT, currentUser
        logMessage = "User " + currentUser.username + " exited."
        return ["221 Successful Quit.", logMessage], LOGGED_OUT, None

    def handlePwdCommand(self, commandTokens, currentUser):#LOG?
        if len(commandTokens) != 1:
            return [SYNTAX_ERROR, ""], LOGGED_IN, currentUser
        return ["257 " + os.getcwd(), ""], LOGGED_IN, currentUser

    def handleMkdCommand(self, commandTokens, currentUser):
        if len(commandTokens) != 2 and len(commandTokens) != 3 or (len(commandTokens) == 3 and commandTokens[1] != "-i"):
            return [SYNTAX_ERROR, ""], LOGGED_IN, currentUser
        if len(commandTokens) == 3:#Create file
            try:
                newFile = open(commandTokens[2], "x")
                logMessage = "User " + currentUser.username + " created file " + commandTokens[2]
                return ["257 " + commandTokens[2] + " created.", logMessage], LOGGED_IN, currentUser
            except IOError:
                return [OTHER_ERROR, ""], LOGGED_IN, currentUser
        try:
            newDirectory = os.mkdir(commandTokens[1])
            logMessage = "User " + currentUser.username + " created directory " + commandTokens[1]
            return ["257 " + commandTokens[1] + " created.", logMessage], LOGGED_IN, currentUser
        except FileExistsError:
            return [OTHER_ERROR, ""], LOGGED_IN, currentUser

    def handleRmdCommand(self, commandTokens, currentUser):
        if len(commandTokens) != 2 and len(commandTokens) != 3 or (len(commandTokens) == 3 and commandTokens[1] != "-f"):
            return [SYNTAX_ERROR, ""], LOGGED_IN, currentUser
        if len(commandTokens) == 2:#Delete file
            if os.path.isfile(commandTokens[1]):
                if not self.isAllowed(currentUser, commandTokens[1]):
                    return [NOT_ALLOWED, ""], LOGGED_IN, currentUser
                os.remove(commandTokens[1])
                logMessage = "User " + currentUser.username + " deleted file " + commandTokens[1]
                return ["250 " + commandTokens[1] + " deleted.", logMessage], LOGGED_IN, currentUser
            return [OTHER_ERROR, ""], LOGGED_IN, currentUser
        path = os.path.join(os.getcwd(),commandTokens[2])
        if os.path.isdir(path):
            shutil.rmtree(path)
            logMessage = "User " + currentUser.username + " deleted directory " + commandTokens[2]
            return ["250 " + commandTokens[2] + " deleted.", logMessage], LOGGED_IN, currentUser
        return [OTHER_ERROR, ""], LOGGED_IN, currentUser

    def handleListCommand(self, commandTokens, currentUser):#LOG?
        if len(commandTokens) != 1:
            return [SYNTAX_ERROR, ""], LOGGED_IN, currentUser
        fileList = os.listdir(os.getcwd())
        response = '\n'.join(fileList)
        return ["226 List transfer done.","", response], LOGGED_IN, currentUser

    def handleCwdCommand(self, commandTokens, currentUser):
        if len(commandTokens) != 2 and len(commandTokens) != 1:
            return [SYNTAX_ERROR, ""], LOGGED_IN, currentUser
        if len(commandTokens) == 2:
            if not os.path.isdir(commandTokens[1]):
                return [OTHER_ERROR, ""], LOGGED_IN, currentUser
            os.chdir(commandTokens[1])
            logMessage = "User " + currentUser.username + " changed directory to " + commandTokens[1]
            return ["250 Successful Change.", logMessage], LOGGED_IN, currentUser
        os.chdir(BASE_DIR)
        logMessage = "User " + currentUser.username + " changed directory to base directory"
        return ["250 Successful Change.", logMessage], LOGGED_IN, currentUser

    def handleHelpCommand(self, commandTokens, currentUser):#LOG?
        if len(commandTokens) != 1:
            return [SYNTAX_ERROR, ""], LOGGED_IN, currentUser
        message = "214\n" 
        message += "USER [name], Its argument is used to specify the user's string. It is used for user authentication.\n" 
        message += "PASS [password], Its argument is used to specify the user's password. It is used for user authentication.\n" 
        message += "PWD, It is used for printing current working directory\n" 
        message += "MKD [flag] [name], Its argument is used to specify the file/directory path. Flag: -i, If present, a new file will be created and otherwise a new directory. It is used for creating a new file or directory.\n" 
        message += "RMD [flag] [name], Its argument is used to specify the file/directory path. Flag: -f, If present, a directory will be removed and otherwise a file. It is used for removing a file or directory.\n" 
        message += "LIST, It is used for printing list of files/directories exist in current working directory\n" 
        message += "CWD [path], Its argument is used to specify the directory's path. It is used for changing the current working directory.\n" 
        message += "DL [name], Its argument is used to specify the file's name. It is used for downloading a file.\n" 
        message += "HELP, It is used for printing list of availibale commands.\n" 
        message += "QUIT, It is used for signing out from the server.\n"
        return [message, ""], LOGGED_IN, currentUser
        
    def handleDlCommand(self, commandTokens, currentUser):
        if len(commandTokens) != 2:
            return [SYNTAX_ERROR, ""], LOGGED_IN, currentUser
        fileName = commandTokens[1]
        path = os.path.join(BASE_DIR,fileName)
        if not os.path.isfile(path):
            return [OTHER_ERROR, ""], LOGGED_IN, currentUser
        if not self.isAllowed(currentUser, fileName):
            return [NOT_ALLOWED, ""], LOGGED_IN, currentUser
        hasEnoughTraffic, currentUser = self.hasTraffic(currentUser, fileName)
        if hasEnoughTraffic:
            fileContent = open(path).read()
            logMessage = "User " + currentUser.username + " downloaded file " + fileName
            return ["226 Successful Download.", logMessage, fileContent], LOGGED_IN, currentUser
        return ["425 Can't open data connection.", ""], LOGGED_IN, currentUser
        
    def executeCommand(self, commandTokens, status, currentUser):
        command = commandTokens[0]
        if status == LOGGED_OUT:
            if command == "USER":
                return self.handleUserCommand(commandTokens, currentUser)
            elif command == "PASS":
                return ["503 Bad sequence of commands."], LOGGED_OUT, currentUser
            else:
                return ["332 Need account for login."], LOGGED_OUT, currentUser
        elif status == WAITING_FOR_PASSWORD:
            if command == "PASS":
                return self.handlePassCommand(commandTokens, currentUser)
            elif command == "QUIT":
                return self.handleQuit(commandTokens, currentUser)
            elif command == "USER":
                return ["503 Bad sequence of commands."], WAITING_FOR_PASSWORD, currentUser
            else:
                return ["332 Need account for login."], WAITING_FOR_PASSWORD, currentUser
        else:
            if command == "PWD":
                return self.handlePwdCommand(commandTokens, currentUser)
            elif command == "MKD":
                return self.handleMkdCommand(commandTokens, currentUser)
            elif command == "RMD":
                return self.handleRmdCommand(commandTokens, currentUser)
            elif command == "LIST":
                return self.handleListCommand(commandTokens, currentUser)
            elif command == "CWD":
                return self.handleCwdCommand(commandTokens, currentUser)
            elif command == "DL":
                return self.handleDlCommand(commandTokens, currentUser)
            elif command == "HELP":
                return self.handleHelpCommand(commandTokens, currentUser)
            elif command == "QUIT":
                return self.handleQuit(commandTokens, currentUser)
            else:
                return [OTHER_ERROR], LOGGED_IN, currentUser

    def sendResponseToClient(self, clientCommandSocket, clientDataSocket, response):
        clientCommandSocket.sendall(response[0].encode('utf-8'))
        if len(response) > 2:
            clientDataSocket.sendall(response[2].encode('utf-8'))

    def handleClient(self, clientCommandSocket):
        status = LOGGED_OUT
        currentUser = None
        clientDataSocket, clientDataAddress = self.serverDataSocket.accept()
        while True:
            command = clientCommandSocket.recv(MAX_SIZE).decode('utf-8')
            print(command)
            commandTokens = command.split(' ')
            response, status, currentUser = self.executeCommand(commandTokens, status, currentUser)
            self.writeLog(response[1])
            self.sendResponseToClient(clientCommandSocket, clientDataSocket, response)

    def startServer(self):
        self.serverCommandSocket = socket(AF_INET, SOCK_STREAM)
        self.serverCommandSocket.bind(("", self.commandChannel))
        self.serverCommandSocket.listen(5)
        self.serverDataSocket = socket(AF_INET, SOCK_STREAM)
        self.serverDataSocket.bind(("", self.dataChannel))
        self.serverDataSocket.listen(5)
        while True:
            clientCommandSocket, clientAddress = self.serverCommandSocket.accept()
            self.writeLog("Client with address " + clientAddress[0] + "/" + str(clientAddress[1]) + " connected.")
            newThread = threading.Thread(target=self.handleClient,args=(clientCommandSocket,))
            newThread.start()


s = Server()

