from socket import *
import json

MAX_SIZE = 2048

class Client:

    def __init__(self):
        self.findServerPort()
        self.startClient()

    def findServerPort(self):
        configFile = open("config.json").read()
        configs = json.loads(configFile)
        self.commandChannel = configs['commandChannelPort']
        self.dataChannel = configs['dataChannelPort']

    def startClient(self):
        self.clientCommandSocket = socket(AF_INET, SOCK_STREAM)
        self.clientCommandSocket.connect(("", self.commandChannel))
        self.clientDataSocket = socket(AF_INET, SOCK_STREAM)
        self.clientDataSocket.connect(("", self.dataChannel))
        while True:
            command = input()
            commandTokens = command.split()
            self.clientCommandSocket.sendall(command.encode('utf-8'))
            response = self.clientCommandSocket.recv(MAX_SIZE).decode('utf-8')
            responseTokens = response.split()
            if (commandTokens[0] == "LIST" or commandTokens[0] == "DL") and responseTokens[0] == "226":
                data = self.clientDataSocket.recv(MAX_SIZE).decode('utf-8')
                if commandTokens[0] == "DL":
                    newFile = open(commandTokens[1], "w")
                    newFile.write(data)
                    newFile.close()
                else:
                    print(data)
            print(response)
        self.close()



newClient = Client()