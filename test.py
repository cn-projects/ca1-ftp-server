import threading
import time

class Test:
    def __init__(self):
        self.items = [1, 2, 3, 4]

    def run(self, count):
        self.items = [count]
        time.sleep(3)
        print("IN CHILD: ", self.items)

    def createThreads(self):
        t = []
        for i in range(5):
            newThread = threading.Thread(target=self.run, args=(i,))
            newThread.start()
            print("IN parent", self.items)
            t.append(newThread)
        for i in t:
            i.join()
        

test = Test()
test.createThreads()
