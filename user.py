import enum

class State(enum.Enum):
    LoggedOut = 1
    WaitingForPassword = 2
    LoggedIn = 3

class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.admin = False

    def setAccountingInfo(self, size, email, alert):
        self.email = email
        self.size = int(size)
        self.alert = alert

    def setAdmin(self):
        self.admin = True
    
